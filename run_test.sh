#!/bin/bash

# Arguments:
# 1. <filesize in gb>
# 2. device (/dev/sda2)
# 3. mountpoint (/mnt/perf-test)
# 4, 5, ... file system types

# For ext3, this test will automatically test with data=ordered and
# data=writeback modes.

# FIXME: For ext4, this test will automatically test with 'nodelalloc'
# as well as the default (delayed alloc enabled)

# Need command-line options for everything described above!

EXPECTED_ARGS=4
TESTNAME="./test-file-zero-alloc-speed"
PATH=$HOME/sbin:$HOME/bin:$PATH
RESULTS_FILE="run_results.txt"
RUN_LOG="run_log.txt"

function mkfs {
    echo $1 $2
    $1 $2 > $RUN_LOG
    if [ $? -ne 0 ]; then
	  echo mkfs error
	  exit -2
    fi
}

function set_ext_parameter {
    echo tune2fs -o $2 $1
    tune2fs -o $2 $1 > $RUN_LOG
    if [ $? -ne 0 ]; then
	echo tune2fs error
	exit -3
    fi
}

function write_frags {
    mount $1 $2 -t $3
    filefrag $2/$3-* 2>/dev/null >> $RESULTS_FILE
    umount $1
}

function do_test {
    echo $TESTNAME $1 $2 $3 $4
    $TESTNAME $1 $2 $3 $4 >> $RESULTS_FILE

    write_frags $2 $3 $4

}

args=("$@")

if [ $# -lt 4 ]; then
    echo "Usage: `basename $0` <filesize-in-GiB> </dev/partition> <mountpoint> <fs1> [<fs2> [<fs3> ...]]"
fi

>$RESULTS_FILE

i=$(expr $EXPECTED_ARGS - 1)
while [ $i -lt $# ]; do
    fs=${args[$i]}

    echo "---------------------------------------------------" >> $RESULTS_FILE
    echo "===============" >> $RESULTS_FILE
    echo $fs >> $RESULTS_FILE
    echo "===============" >> $RESULTS_FILE

    case $fs in
	"ext2")
	    mkfs "mkfs.ext2" $2
	    do_test $1 $2 $3 $fs
	    ;;

	"ext3")
	    echo "**	journal_data_writeback" >> $RESULTS_FILE
	    mkfs "mkfs.ext3" $2
	    set_ext_parameter $2 "journal_data_writeback"
	    do_test $1 $2 $3 $fs

	    echo "**	journal_data_ordered" >> $RESULTS_FILE
	    mkfs "mkfs.ext3" $2
	    set_ext_parameter $2 "journal_data_ordered"
	    do_test $1 $2 $3 $fs
	    ;;

	"ext4")
	    mkfs "mkfs.ext4" $2
	    do_test $1 $2 $3 $fs
	    ;;

	"xfs")
	    mkfs "mkfs.xfs -f" $2
	    do_test $1 $2 $3 $fs
	    ;;

	"btrfs")
	    mkfs "mkfs.btrfs" $2
	    do_test $1 $2 $3 $fs
	    ;;

	"reiserfs")
	    mkfs "mkreiserfs -q" $2
	    do_test $1 $2 $3 $fs
	    ;;
    esac

    i=$(expr $i + 1)
done

exit 0
