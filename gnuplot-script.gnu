# set terminal png transparent nocrop enhanced font arial 8 size 420,320 
# set output 'histograms.2.png'
# Script from http://gnuplot.sourceforge.net/demo_4.1/histograms.html

set title "Performance of Allocating a Zeroed File on Different File Systems - 4GiB file"
set boxwidth 0.9 absolute
set style fill   solid 1.00 border -1
set style histogram clustered gap 1 title  offset character 0, 0, 0
set datafile missing '-'
set style data histograms
set xtics border in scale 1,0.5 nomirror rotate by -45  offset character 0, 0, 0 
set xlabel "File System"
set ylabel "Seconds"
plot 'run_results.txt' using 2:xtic(1) ti col, '' u 3 ti col, '' u 4 ti col, '' u 5 ti col

pause -1

set ylabel "Fragments"
plot 'run_results.txt' using 6:xtic(1) ti col, '' u 7 ti col, '' u 8 ti col, '' u 9 ti col
