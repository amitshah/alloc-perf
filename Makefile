# (C) Copyright 2009 Red Hat Inc.
# Author: Amit Shah <amit.shah@redhat.com>

CC = gcc
CFLAGS = -Werror -g

.PHONY: all

all: test-file-zero-alloc-speed

clean:
	rm -f *.o *~ test-file-zero-alloc-speed

